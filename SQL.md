# What is SQL?


**SQL** stands for **Structured Query Language**. SQL is a type of a programming language that is used to **retrieve information from databases**. SQL is the language of databases.

![SQL image](https://www.geeksforgeeks.org/wp-content/uploads/SQL.png)

## SQL Basics
* SQL is based on relational algebra **(RDBMS)**.
* RDBMS stands for **Relational Database Management System**.
* The data in RDBMS is stored in database objects called **Tables**. 
* A table is a collection of **columns** and **rows**.
* **Tuple:** It is a row. One row in a table is known as a tuple. 
* **Attribute:** It is a column of a table. Degree: Number of columns in a table. 
* **Cardinality:** Number of rows in a table.
* It is pronounced as **S-Q-L** or sometime **See-Qwell**.
* SQL is just a query language. it is not a database.
* To perform SQL queries, you need to install any database, for example, **Oracle**, **MySQL**, **MongoDB**, **PostGre SQL**, **SQL Server** etc.


## Need of SQL

* To create new databases.
* To create tables in a database.
* To insert records in a database.
* To update records in a database.
* To delete records from a database.
* To retrieve data from a database.


## SQL Commands

These are the some important SQL command:
* **CREATE DATABASE**
    * To create a new database.
    ```SQL
          CREATE DATABASE databasename;
    ```
*  **CREATE TABLE**
    * To create a new table.
   ``` SQL
        CREATE TABLE table_name (column1 datatype, column2 datatype, column3 datatype, .... ); 
    ```
* **INSERT INTO**
    * it inserts new data into a database.
    ```SQL
        INSERT INTO table_name (column1, column2, column3, ...) VALUES (value1, value2, value3, ...);
    ```

* **SELECT**
    * To retrieve data from a database.
    ``` SQL
        SELECT "column_name" FROM "table_name";  
    ```

* **UPDATE**
    * To update from a database.
    ``` SQL
        UPDATE table_name SET column1 = value1, column2 = value2, ... 
        WHERE condition;
    ```


* **DELETE**
    * To delete record the from table.
    ``` SQL
        DELETE FROM table_name WHERE condition;
    ```

* **ALTER TABLE** 
    * To modify the table.
    ``` SQL
        ALTER TABLE table_name ADD column_name datatype;  
    ```
* **DROP TABLE**
    * To delete a table.
    ``` SQL
        DROP TABLE table_name; 
    ```    
* **DROP DATABASE**
    * To delete a database.
    ``` SQL
        DROP DATABASE databasename;
    ```


## JOINS

---
Join is the SQL statement with the help of which we can combine different tables. Joins can only work when there is at least one common attribute in 2 tables.

### Types of Joins

* **(Inner) Join**
  * Returns records that have matching values in both tables.

* **Left (Outer) Join**
  * Returns all records from the left table, and the matched records from the right table.

* **Right (Outer) Join**
  * Returns all records from the right table, and the matched records from the left table.

* **Full (Outer) Join**
  * Returns all records when there is a match in either left or right table.

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSZNAfZfNO3LOCuuXZiyPCh32r726AS5OKamQ&usqp=CAU)

Let an example to deploy SQL JOIN process:

* **Staff table**

    | ID | Staff_Name | Staff_AGE | STAFF_ADDRESS | Monthley_Package |
    |:-------:|:-----------:|:------:|:----------:|:-------------:|
    | 1 | ANJALI | 22 | MUMBAI | 18000 |
    |2	| NISHA	| 29	| DELHI	|20000 |
    |3	| KAVITA	| 25	| MOHALI	|22000 |
    |4	| AKASH	| 24	| ALLAHABAD	| 12000 |



* **Payment table**
			
    | Payment_ID | DATE | Staff_ID | AMOUNT |
    |:-------:|:-----------:|:------:|:----:|
    | 101 | 30/12/2009 | 1 | 3000.00 |
    | 102 | 22/02/2010 | 3 | 2500.00 |
    | 103 | 23/02/2010 | 4 | 3500.00 |

    So if you follow this JOIN statement to join these two tables ?

    ```
    SELECT Staff_ID, Staff_NAME, Staff_AGE, AMOUNT FROM STAFF s, PAYMENT p 
    WHERE s.ID =p.STAFF_ID;  
    ```

     This will produce the result like this

    | STAFF_ID | NAME | Staff_AGE | AMOUNT |
    |:--------:|:----:|:---------:|:------:|
    | 3 | KAVITA | 25 | 2500.00 |
    | 1 | ANJALI | 22 | 3000.00 |
    | 4 | AKASH | 24 | 3500.00 |
    | 1 | ANJALI | 22 | 3000.00 |

## Aggregate functions
---
There are some aggregate functions:

* COUNT()
* Sum()
* Avg()
* Min()
* Max()

Let's take an example of **Salary_Table** :-

|Id     |Name     |Salary |
|:-----:|:-------:|:-----:|
|1      |A        |80
|2      |B        |40
|3      |C        |60
|4      |D        |70
|5      |E        |60
|6      |F        |Null

**COUNT():**

```SQL
SELECT COUNT(column_name) FROM table_name WHERE condition; //Syntax

```
```SQL
SELECT COUNT(*) FROM Salary_Table; // 6,  It will return total no. of records in the table.

SELECT COUNT(salary) FROM Salary_Table WHERE salary = 60; // 5, It will return the no. of Non Null values in the column salary.
```

**SUM():**


```SQL
SELECT SUM(column_name) FROM table_name WHERE condition; //Syntax

```
```SQL
SELECT SUM(salary) FROM Salary_Table; // 310, It will return the sum of all Non Null values in the column salary.
```

**AVG():**

```SQL
SELECT AVG(column_name) FROM table_name WHERE condition; //Syntax
```

```SQL
SELECT AVG(salary) FROM Salary_Table WHERE condition; // 310/5 , It will return the avg of the column salary.
```

**MIN():**

```SQL
SELECT MIN(column_name) FROM table_name WHERE condition; //Syntax
```

```SQL
SELECT MIN(salary) FROM Salary_Table WHERE condition; // 40, It will return the minimum value of the column salary except NULL.
```

**MAX():**

```SQL
SELECT MAX(column_name) FROM table_name WHERE condition; //Syntax
```

```SQL
SELECT MAX(salary) FROM Salary_Table WHERE condition; // 80, It will return the maximum value of the column salary.
```

**References**

1. [w3schools](https://www.w3schools.com/sql/sql_intro.asp)
1. [javatpoint](https://www.javatpoint.com/sql-join)
